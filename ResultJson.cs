﻿using System;

namespace Altyapi.Data
{
    public class ResultJson
    {
        public String Title { get; set; }
        public String MType { get; set; }
        public String Message { get; set; }
        public String RedirectURL { get; set; }

    }
}