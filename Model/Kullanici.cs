﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Text;

namespace Altyapi.Data.Model
{
    [Table("KULLANICI")]
    public class Kullanici
    {
        public Kullanici()
        {
            this.Roller = new List<Rol>();
        }

        [Key]
        public int ID { get; set; }

        private DateTime ETarih = DateTime.Now;
        private DateTime DTarih = DateTime.Now;

        [Display(Name = "Eklenme Tarihi : ")]
        public DateTime EklenmeTarihi { get { return ETarih; } set { ETarih = value; } }

        [Display(Name = "Değiştirme Tarihi : ")]
        public DateTime DegistirmeTarihi { get { return DTarih; } set { DTarih = value; } }

        private bool Aktif = true;

        public bool AktifMi { get { return Aktif; } set { Aktif = value; } }

        private string _kullaniciAdi;

        public string KullaniciAdi
        {
            get { return _kullaniciAdi; }
            set { _kullaniciAdi = value.ToLowerInvariant(); }
        }

        public string AdSoyad { get; set; }

        public string Eposta { get; set; }

        public string Sifre { get; set; }

        public void SetPassword(string password)
        {
            Sifre = Crypto.Hash(password);
        }

        public bool CheckPassword(string password)
        {
            return string.Equals(Sifre, Crypto.Hash(password));
        }

        public string ResimKucuk { get; set; }

        public string ResimBuyuk { get; set; }

        public string SeoSlug { get; set; }

        public DateTime SonGirisTarihi { get; set; }

        public string SonGirisIP { get; set; }

        [Display(Name = "DomainName")]
        public string DomainName { get; set; } = ConfigurationManager.AppSettings["DomainName"];

        public virtual ICollection<Rol> Roller { get; set; }
    }

    public static class Crypto
    {
        public static string Hash(string value)
        {
            return Convert.ToBase64String(
                System.Security.Cryptography.SHA256.Create()
                .ComputeHash(Encoding.UTF8.GetBytes(value)));
        }
    }


}
