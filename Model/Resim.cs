﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Altyapi.Data.Model
{
    [Table("RESIM")]
    public class Resim
    {
        [Key]
        public int ID { get; set; }

        public string ResimUrlKucuk { get; set; }

        public string ResimUrlBuyuk { get; set; }

        public int Siralama { get; set; }

        public int YayinId { get; set; }

        public virtual Yayin Yayin { get; set; }
    }
}
