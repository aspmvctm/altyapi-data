﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Altyapi.Data.Base;

namespace Altyapi.Data.Model
{
    [Table("YAYIN")]
    public class Yayin : BaseEntity
    {
        public Yayin()
        {
            Etiketler = new List<Etiket>();
            Resimler = new List<Resim>();
        }

        public string Baslik { get; set; }

        public string KisaAciklama { get; set; }

        public string Icerik { get; set; }

        public string SeoSlug { get; set; }

        public int Siralama { get; set; }

        public string EtiketAdlari { get; set; }

        public string ResimUrlKucuk { get; set; }

        public string ResimUrlBuyuk { get; set; }

        public int OkunmaSayisi { get; set; }

        public int KategoriId { get; set; }

        public DateTime YayinTarihi { get; set; }

        public bool SlayttaGorunsunMu { get; set; }

        public virtual Kategori Kategori { get; set; }

        public virtual ICollection<Resim> Resimler { get; set; }

        public virtual ICollection<Etiket> Etiketler { get; set; }
    }
}
