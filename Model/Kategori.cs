﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Altyapi.Data.Base;

namespace Altyapi.Data.Model
{
    [Table("KATEGORI")]
    public class Kategori : BaseRefEntity
    {
        public Kategori()
        {
            Yayinlar = new List<Yayin>();
        }

        public string SeoSlug { get; set; }
        public int Siralama { get; set; }

        public virtual ICollection<Yayin> Yayinlar { get; set; }
    }
}
