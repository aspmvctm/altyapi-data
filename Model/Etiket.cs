﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Altyapi.Data.Base;

namespace Altyapi.Data.Model
{
    [Table("ETIKET")]
    public class Etiket : BaseRefEntity
    {
        public Etiket()
        {
            Yayinlar = new HashSet<Yayin>();
        }

        public string SeoSlug { get; set; }

        public virtual ICollection<Yayin> Yayinlar { get; set; }
    }
}
