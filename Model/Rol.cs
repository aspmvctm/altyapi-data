﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Altyapi.Data.Base;

namespace Altyapi.Data.Model
{
    [Table("ROL")]
    public class Rol : BaseRefEntity
    {
        public Rol()
        {
            this.Kullanicilar = new List<Kullanici>();
        }

        public virtual ICollection<Kullanici> Kullanicilar { get; set; }
    }
}
