﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Altyapi.Data.Base;

namespace Altyapi.Data.ViewModel
{
    public class ViewModelKategori
    {
        public int ID { get; set; }
        public string Ad { get; set; }
        public bool Durumu = true;
        public int Siralama { get; set; }
    }
}
