﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Altyapi.Data.Model;

namespace Altyapi.Data.ViewModel
{
    public class ViewModelSlaytYayin
    {
        public int ID { get; set; }

        public string KategoriAdi { get; set; }

        [Required(ErrorMessage = "Herhangi bir kategori türü seçiniz.")]
        public int KategoriID { get; set; }

        [MaxLength(100, ErrorMessage = "100 karakterden fazla girmeyiniz.")]
        [MinLength(3, ErrorMessage = "3 karakterden az girmeyiniz.")]
        [Display(Name = "Yayın Başlığı")]
        [Required]
        public string Baslik { get; set; }

        [MaxLength(255, ErrorMessage = "255 karakterden fazla girmeyiniz.")]
        [Display(Name = "Kısa Açıklama(Bağlantıda Görünecek)")]
        public string KisaAciklama { get; set; }

        public string ResimKucuk { get; set; }

        public string ResimBuyuk { get; set; }

        public bool SlayttaGorunsunMu = true;

        public int Siralama { get; set; }

        public string SeoSlug { get; set; }

        public string SeoSlugKategori { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.mm.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime YayinTarihi { get; set; }

    }
}
