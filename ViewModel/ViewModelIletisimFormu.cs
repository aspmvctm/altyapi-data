﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Altyapi.Data.ViewModel
{
    public class ViewModelIletisimFormu
    {
        public string Ad { get; set; }
        public string Eposta { get; set; }
        public string Konu { get; set; }
        public string Mesaj { get; set; }
    }
}
