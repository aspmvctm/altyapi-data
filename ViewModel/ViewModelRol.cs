﻿namespace Altyapi.Data.ViewModel
{
    public class ViewModelRol
    {
        public int ID { get; set; }
        public string Ad { get; set; }
        public bool Durumu = true;
    }
}
