﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Altyapi.Data.Model;

namespace Altyapi.Data.ViewModel
{
    public class ViewModelKullanici
    {
        public ViewModelKullanici()
        {
                Roller=new List<Rol>();
        }

        public int ID { get; set; }

        public HttpPostedFileBase KullaniciResim { get; set; }

        public string ResimKucuk { get; set; }

        public string ResimBuyuk { get; set; }

        public string SeoSlug { get; set; }

        [MinLength(3, ErrorMessage = "3 karakterden fazla değer giriniz."), MaxLength(20, ErrorMessage = "20 karakterden fazla girmeyiniz.")]
        [Display(Name = "Kullanici Adi")]
        public string KullaniciAdi { get; set; }

        [MaxLength(50, ErrorMessage = "50 karakterden fazla girmeyiniz.")]
        [Display(Name = "Adı Soyadı")]
        public string AdiSoyadi { get; set; }

        [Display(Name = "E-Posta")]
        [EmailAddress(ErrorMessage = "Geçersiz e-posta adresi.")]
        [Required]
        public string Eposta { get; set; }


        [Display(Name = "Aktif Mi ?")]
        public bool Durumu = true;
        public int Siralama { get; set; }

        public List<Rol> Roller { get; set; }
    }
}
