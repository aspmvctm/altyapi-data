﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Altyapi.Data.Model;

namespace Altyapi.Data.ViewModel
{
    public class ViewModelKullaniciSifre
    {
        public int ID { get; set; }

        [Display(Name = "Şifre")]
        [DataType(DataType.Password)]
        [Required]
        public string Sifre { get; set; }

        [Display(Name = "Yeni Şifre")]
        [DataType(DataType.Password)]
        [Required]
        public string YeniSifre { get; set; }

        [Display(Name = "Yeni Şifre Tekrar")]
        [Compare("YeniSifre", ErrorMessage = "Şifre Eşleşmiyor.Lütfen tekrar giriniz!")]
        [DataType(DataType.Password)]
        [Required]
        public string YeniSifreTekrar { get; set; }

        public void SetPassword(string password)
        {
            YeniSifre = Crypto.Hash(password);
        }

        public bool CheckPassword(string password)
        {
            return string.Equals(YeniSifre, Crypto.Hash(password));
        }
    }
}
