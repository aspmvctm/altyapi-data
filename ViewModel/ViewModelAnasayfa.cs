﻿using System.Collections.Generic;

namespace Altyapi.Data.ViewModel
{
    public class ViewModelAnasayfa
    {
        public IEnumerable<ViewModelSlaytYayin> ViewModelSlaytYayin { get; set; }

        public IEnumerable<ViewModelYayin> ViewModelYayin { get; set; }
    }
}
