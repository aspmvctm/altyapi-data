﻿using System.ComponentModel.DataAnnotations;

namespace Altyapi.Data.Base
{
    public class BaseRefEntity : BaseEntity
    {
        [Display(Name = "Adı : ")]
        [MinLength(3,ErrorMessage ="3 karakterden fazla değer giriniz."),MaxLength(150,ErrorMessage ="150 karakterden fazla karakter girmeyiniz.")]
        public string Ad { get; set; }
	}
}
