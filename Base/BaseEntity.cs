﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;

namespace Altyapi.Data.Base
{
    public class BaseEntity
    {
        [Key]
        public int ID { get; set; }

        [Display(Name ="Eklenme Tarihi : ")]
        public DateTime EklenmeTarihi { get; set; } = DateTime.Now;

        [Display(Name = "Değiştirme Tarihi : ")]
        public DateTime DegistirmeTarihi { get; set; } = DateTime.Now;

        public int EkleyenKullaniciId { get; set; }

        public int? GuncelleyenKullaniciId { get; set; }

        [Display(Name = "Aktif Mi ?")]
        public bool AktifMi { get; set; } = true;

        [Display(Name = "Versiyon")]
        public int? Versiyon { get; set; }

        [Display(Name = "DomainName")]
        public string DomainName { get; set; }=  ConfigurationManager.AppSettings["DomainName"];
    }
}
