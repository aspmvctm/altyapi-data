﻿using System.Data.Entity;
using Altyapi.Data.Model;
using System.Configuration;

namespace Altyapi.Data.DataContext
{
    public class AltyapiContext: DbContext
    {
        public AltyapiContext()
            : base(ConfigurationManager.AppSettings["VeritabaniAdi"])
        {
            Configuration.LazyLoadingEnabled = true;
        }

        public DbSet<Kullanici> Kullanici { get; set;}

        public DbSet<Rol> Rol { get; set; }

        public DbSet<Resim> Resim { get; set; }

        public DbSet<Yayin> Yayin { get; set; }

        public DbSet<Kategori> Kategori { get; set; }
         
        public DbSet<Etiket> Etiket { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Kullanici>()
            .HasMany<Rol>(s => s.Roller)
            .WithMany(c => c.Kullanicilar)
            .Map(cs =>
            {
                cs.MapLeftKey("KullaniciId");
                cs.MapRightKey("RolId");
                cs.ToTable("KULLANICIROL");
            });

            modelBuilder.Entity<Yayin>()
               .HasMany(u => u.Resimler)
               .WithRequired()
               .HasForeignKey(h => h.YayinId);

        }
    }
}
