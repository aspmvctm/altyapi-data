namespace Altyapi.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Olustur : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ETIKET",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SeoSlug = c.String(),
                        Ad = c.String(maxLength: 150),
                        EklenmeTarihi = c.DateTime(nullable: false),
                        DegistirmeTarihi = c.DateTime(nullable: false),
                        EkleyenKullaniciId = c.Int(nullable: false),
                        GuncelleyenKullaniciId = c.Int(),
                        AktifMi = c.Boolean(nullable: false),
                        Versiyon = c.Int(),
                        DomainName = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.YAYIN",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Baslik = c.String(),
                        KisaAciklama = c.String(),
                        Icerik = c.String(),
                        SeoSlug = c.String(),
                        Siralama = c.Int(nullable: false),
                        EtiketAdlari = c.String(),
                        ResimUrlKucuk = c.String(),
                        ResimUrlBuyuk = c.String(),
                        OkunmaSayisi = c.Int(nullable: false),
                        KategoriId = c.Int(nullable: false),
                        YayinTarihi = c.DateTime(nullable: false),
                        SlayttaGorunsunMu = c.Boolean(nullable: false),
                        EklenmeTarihi = c.DateTime(nullable: false),
                        DegistirmeTarihi = c.DateTime(nullable: false),
                        EkleyenKullaniciId = c.Int(nullable: false),
                        GuncelleyenKullaniciId = c.Int(),
                        AktifMi = c.Boolean(nullable: false),
                        Versiyon = c.Int(),
                        DomainName = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.KATEGORI", t => t.KategoriId, cascadeDelete: true)
                .Index(t => t.KategoriId);
            
            CreateTable(
                "dbo.KATEGORI",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SeoSlug = c.String(),
                        Siralama = c.Int(nullable: false),
                        Ad = c.String(maxLength: 150),
                        EklenmeTarihi = c.DateTime(nullable: false),
                        DegistirmeTarihi = c.DateTime(nullable: false),
                        EkleyenKullaniciId = c.Int(nullable: false),
                        GuncelleyenKullaniciId = c.Int(),
                        AktifMi = c.Boolean(nullable: false),
                        Versiyon = c.Int(),
                        DomainName = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.RESIM",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ResimUrlKucuk = c.String(),
                        ResimUrlBuyuk = c.String(),
                        Siralama = c.Int(nullable: false),
                        YayinId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.YAYIN", t => t.YayinId, cascadeDelete: true)
                .Index(t => t.YayinId);
            
            CreateTable(
                "dbo.KULLANICI",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        EklenmeTarihi = c.DateTime(nullable: false),
                        DegistirmeTarihi = c.DateTime(nullable: false),
                        AktifMi = c.Boolean(nullable: false),
                        KullaniciAdi = c.String(),
                        AdSoyad = c.String(),
                        Eposta = c.String(),
                        Sifre = c.String(),
                        ResimKucuk = c.String(),
                        ResimBuyuk = c.String(),
                        SeoSlug = c.String(),
                        SonGirisTarihi = c.DateTime(nullable: false),
                        SonGirisIP = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ROL",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Ad = c.String(maxLength: 150),
                        EklenmeTarihi = c.DateTime(nullable: false),
                        DegistirmeTarihi = c.DateTime(nullable: false),
                        EkleyenKullaniciId = c.Int(nullable: false),
                        GuncelleyenKullaniciId = c.Int(),
                        AktifMi = c.Boolean(nullable: false),
                        Versiyon = c.Int(),
                        DomainName = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.YayinEtikets",
                c => new
                    {
                        Yayin_ID = c.Int(nullable: false),
                        Etiket_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Yayin_ID, t.Etiket_ID })
                .ForeignKey("dbo.YAYIN", t => t.Yayin_ID, cascadeDelete: true)
                .ForeignKey("dbo.ETIKET", t => t.Etiket_ID, cascadeDelete: true)
                .Index(t => t.Yayin_ID)
                .Index(t => t.Etiket_ID);
            
            CreateTable(
                "dbo.KULLANICIROL",
                c => new
                    {
                        KullaniciId = c.Int(nullable: false),
                        RolId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.KullaniciId, t.RolId })
                .ForeignKey("dbo.KULLANICI", t => t.KullaniciId, cascadeDelete: true)
                .ForeignKey("dbo.ROL", t => t.RolId, cascadeDelete: true)
                .Index(t => t.KullaniciId)
                .Index(t => t.RolId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.KULLANICIROL", "RolId", "dbo.ROL");
            DropForeignKey("dbo.KULLANICIROL", "KullaniciId", "dbo.KULLANICI");
            DropForeignKey("dbo.RESIM", "YayinId", "dbo.YAYIN");
            DropForeignKey("dbo.YAYIN", "KategoriId", "dbo.KATEGORI");
            DropForeignKey("dbo.YayinEtikets", "Etiket_ID", "dbo.ETIKET");
            DropForeignKey("dbo.YayinEtikets", "Yayin_ID", "dbo.YAYIN");
            DropIndex("dbo.KULLANICIROL", new[] { "RolId" });
            DropIndex("dbo.KULLANICIROL", new[] { "KullaniciId" });
            DropIndex("dbo.YayinEtikets", new[] { "Etiket_ID" });
            DropIndex("dbo.YayinEtikets", new[] { "Yayin_ID" });
            DropIndex("dbo.RESIM", new[] { "YayinId" });
            DropIndex("dbo.YAYIN", new[] { "KategoriId" });
            DropTable("dbo.KULLANICIROL");
            DropTable("dbo.YayinEtikets");
            DropTable("dbo.ROL");
            DropTable("dbo.KULLANICI");
            DropTable("dbo.RESIM");
            DropTable("dbo.KATEGORI");
            DropTable("dbo.YAYIN");
            DropTable("dbo.ETIKET");
        }
    }
}
