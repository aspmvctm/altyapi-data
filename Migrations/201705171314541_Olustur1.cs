namespace Altyapi.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Olustur1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.KULLANICI", "DomainName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.KULLANICI", "DomainName");
        }
    }
}
